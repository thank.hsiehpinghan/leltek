// @flow

import React, {Component} from 'react'

import { Page, Card, Grid, Form, Button, Dropdown } from "tabler-react";

import ComponentDemo from "./ComponentDemo";
import SiteWrapper from "./SiteWrapper.react";
import Carousel, { Modal, ModalGateway } from 'react-images';

class FormElements extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      modalIsOpen: false
    }
  }

  componentDidMount() {
	  let token = window.$token;
    fetch('/api/imagesList', {
	  method: "GET",
	  headers: {
		  token: token
	  }
	})
    .then(res => res.json())
    .then(data => {
        let images = data.map((item) => {return {source: "/api/images/" + item}});
        this.setState({
          data: data,
          images: images
        })


    })
    .catch(e => console.log('错误:', e))
  }


  toggleModal = (e) => {
    this.setState(state => ({ modalIsOpen: !state.modalIsOpen }));
    e.preventDefault();
  }

  
  render() {
    const images = [
      { source: 'https://peach.blender.org/wp-content/uploads/bbb-splash.png' }, 
      { source: 'https://durian.blender.org/wp-content/uploads/2010/06/02.b_comp_000296.jpg' }
    ];
    return (
      <SiteWrapper>
        <Page.Card
          title="下載"
          RootComponent={Form}
          footer={
            <Card.Footer>
              <div className="d-flex">
                <Button link>取消</Button>
                <Button link color="info" className="ml-auto" onClick={(e) => {this.toggleModal(e)}} >
                  瀏覽
                </Button>
                <Button type="submit" color="primary" className="ml-auto">
                  下載
                </Button>
              </div>
            </Card.Footer>
          }
        >
                <Form.Group>
                  <Form.ImageCheck>

                {this.state.data &&
                    this.state.data.map((item, index) => (
                      <Form.ImageCheckItem
                        key={item}
                        value={item}
                        imageURL={"/api/images/" + item}
                      />
                    ))}

                  </Form.ImageCheck>
                </Form.Group>
        </Page.Card>
      <ModalGateway>
        {(this.state.modalIsOpen && this.state.images) ? (
          <Modal onClose={this.toggleModal}>
            <Carousel views={this.state.images} />
          </Modal>
        ) : null}
      </ModalGateway>
      </SiteWrapper>
    );
  }
}

export default FormElements;
