// @flow

import * as React from "react";

import { Page, Grid, PricingCard } from "tabler-react";

import SiteWrapper from "../MySiteWrapper.react";

function MyPricingCardsPage(): React.Node {
  return (
    <SiteWrapper>
      <Page.Content title="優惠方案">
        <Grid.Row>
          <Grid.Col sm={6} lg={3}>
            <PricingCard>
              <PricingCard.Category>{"入門方案"}</PricingCard.Category>
              <PricingCard.Price>{"$1,200"}</PricingCard.Price>
              <PricingCard.AttributeList>
                <PricingCard.AttributeItem>
                  <strong>3 </strong>
                  {"位使用者"}
                </PricingCard.AttributeItem>
                <PricingCard.AttributeItem hasIcon available>
                  <strong>100GB </strong>
                  {"儲存空間"}
                </PricingCard.AttributeItem>
                <PricingCard.AttributeItem hasIcon available={false}>
                  {"圖片處理"}
                </PricingCard.AttributeItem>
                <PricingCard.AttributeItem hasIcon available={false}>
                  {"訊息通知"}
                </PricingCard.AttributeItem>
                <PricingCard.AttributeItem hasIcon available={false}>
                  {"上傳facebook"}
                </PricingCard.AttributeItem>
              </PricingCard.AttributeList>
              <PricingCard.Button> {"選擇"} </PricingCard.Button>
            </PricingCard>
          </Grid.Col>

          <Grid.Col sm={6} lg={3}>
            <PricingCard active>
              <PricingCard.Category>{"進階方案"}</PricingCard.Category>
              <PricingCard.Price>{"$5,200"}</PricingCard.Price>
	          <PricingCard.AttributeList>
	              <PricingCard.AttributeItem>
	                <strong>10 </strong>
	                {"位使用者"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available>
	                <strong>1,000GB </strong>
	                {"儲存空間"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available>>
	                {"圖片處理"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available={false}>
	                {"訊息通知"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available={false}>
	                {"上傳facebook"}
	              </PricingCard.AttributeItem>
	          </PricingCard.AttributeList>
              <PricingCard.Button active>{"選擇"} </PricingCard.Button>
            </PricingCard>
          </Grid.Col>

          <Grid.Col sm={6} lg={3}>
            <PricingCard>
              <PricingCard.Category>{"企業方案"}</PricingCard.Category>
              <PricingCard.Price>{"$12,000"}</PricingCard.Price>
	          <PricingCard.AttributeList>
	              <PricingCard.AttributeItem>
	                <strong>20 </strong>
	                {"位使用者"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available>
	                <strong>3,000GB </strong>
	                {"儲存空間"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available>>
	                {"圖片處理"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available>
	                {"訊息通知"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available={false}>
	                {"上傳facebook"}
	              </PricingCard.AttributeItem>
	          </PricingCard.AttributeList>
              
              <PricingCard.Button>{"選擇"} </PricingCard.Button>
            </PricingCard>
          </Grid.Col>

          <Grid.Col sm={6} lg={3}>
            <PricingCard>
              <PricingCard.Category>{"頂級方案"}</PricingCard.Category>
              <PricingCard.Price>{"$30,000"}</PricingCard.Price>
	          <PricingCard.AttributeList>
	              <PricingCard.AttributeItem>
	                <strong>50 </strong>
	                {"位使用者"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available>
	                <strong>10,000GB </strong>
	                {"儲存空間"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available>>
	                {"圖片處理"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available>
	                {"訊息通知"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available>
	                {"上傳facebook"}
	              </PricingCard.AttributeItem>
	          </PricingCard.AttributeList>
              <PricingCard.Button>{"選擇"} </PricingCard.Button>
            </PricingCard>
          </Grid.Col>
        </Grid.Row>
      </Page.Content>
    </SiteWrapper>
  );
}

export default MyPricingCardsPage;
