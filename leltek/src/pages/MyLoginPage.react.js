// @flow

import * as React from "react";
import { Formik } from "formik";
import { LoginPage as TablerLoginPage } from "tabler-react";

class MyLoginPage extends React.Component {
  constructor (props, context) {
    super(props, context);
    this.state = {
    	userId: '',
    	password: ''
    };
  }

	onSubmit = async (e) => {
		e.preventDefault();
		let response = await fetch("/api/login", {
		  method: "POST",
		  body: JSON.stringify(this.state),
		  headers: {
			  'Content-Type': 'application/json'
		  }
		});
		let result = await response.json();
		if(result.code === 200) {
			window.$token = result.token;
			this.props.history.push('/my-home');
		} else {
			console.log(result.code);
		}
	}
	
	onChange(e) {
		let name = e.target.name;
		let value = e.target.value;
		if(name === 'email') {
			  this.setState({
				  userId: value
		      });
		} else if(name === 'password') {
			  this.setState({
				  password: value
		      });
		}
	}
	
  render () {
    return (
    	<TablerLoginPage
	    	action={void(0)}
		    onSubmit={this.onSubmit.bind(this)}
    		onChange={e => this.onChange(e)}
            strings={{
              title: "登入",
              buttonText: "送出",
              emailLabel: "電子信箱",
              emailPlaceholder: "請輸入電子信箱",
              passwordLabel: "密碼",
              passwordPlaceholder: "請輸入密碼",
            }}
          />
    );
  }
}

export default MyLoginPage;
