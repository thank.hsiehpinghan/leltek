// @flow

import * as React from "react";

import { RegisterPage as TablerRegisterPage } from "tabler-react";

class MyRegisterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    	userId: '',
    	password: ''
    };
  }

	onSubmit = async (e) => {
		e.preventDefault();
		let response = await fetch("/api/register", {
		  method: "POST",
		  body: JSON.stringify(this.state),
		  headers: {
			  'Content-Type': 'application/json'
		  }
		});
		let result = await response.json();
		if(result.code === 200) {
			this.props.history.push('/my-login');
		} else {
			console.log(result.code);
		}
	}
	
	onChange(e) {
		let name = e.target.name;
		let value = e.target.value;
		if(name === 'email') {
			  this.setState({
				  userId: value
		      });
		} else if(name === 'password') {
			  this.setState({
				  password: value
		      });
		}
	}

	render() {
	  return (
			<TablerRegisterPage
		  	action={void(0)}
		    onSubmit={this.onSubmit.bind(this)}
			onChange={e => this.onChange(e)}
		    strings={{
		      title: "註冊",
		      buttonText: "送出",
		      nameLabel: "帳號",
		      namePlaceholder: "請輸入帳號",
		      emailLabel: "電子信箱",
		      emailPlaceholder: "請輸入電子信箱",
		      passwordLabel: "密碼",
		      passwordPlaceholder: "請輸入密碼",
		      termsLabel: "同意授權條款",
		    }}
		  /> 
	  );
	}

}

export default MyRegisterPage;
