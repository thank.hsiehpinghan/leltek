import React from 'react';
import Uppy from '@uppy/core';
import Tus from '@uppy/tus';
import GoogleDrive from '@uppy/google-drive';
import Dropbox from '@uppy/dropbox';
import Instagram from '@uppy/instagram';
import Facebook from '@uppy/facebook';
import Webcam from '@uppy/webcam';
import zh_TW from '@uppy/locales/lib/zh_TW'
import { Page } from "tabler-react";
import SiteWrapper from "../SiteWrapper.react";
import './uppy.min.css';

// import Dashboard from '@uppy/dashboard';
import { Dashboard, DashboardModal, DragDrop, ProgressBar } from '@uppy/react';
import { Card, Grid, Form, Button, Dropdown } from "tabler-react";

class MyImageUploadPage extends React.Component {
  constructor (props) {
    super(props)
    let token = window.$token;
    this.uppy = new Uppy({ 
      id: 'uppy1', 
      autoProceed: false, 
      debug: true,
      restrictions: {
        maxNumberOfFiles: 10,
        minNumberOfFiles: 1,
        allowedFileTypes: ['image/*']
      }
     })
	.use(Tus, { 
		endpoint: '/api/images',
		headers: {
			token: token
		}
	})  
  }

  componentWillUnmount () {
    this.uppy.close()
  }

  render () {
    return (
      <SiteWrapper>
      <Page.Card
        title="上傳"
        RootComponent={Form}
        footer={
          <Card.Footer>
            <div className="d-flex">
              <Button link>取消</Button>
              <Button type="submit" color="primary" className="ml-auto">
                送出
              </Button>
            </div>
          </Card.Footer>
        }
      >
      <div>
          <Dashboard
            uppy={this.uppy}
            plugins={['GoogleDrive', 'Dropbox', 'Instagram', 'Facebook', 'Webcam']}
            metaFields={[
              { id: 'name', name: '名稱', placeholder: '請輸入名稱' },
              { id: 'describe', name: '說明', placeholder: '請輸入說明' }
            ]}
            locale={zh_TW}
            proudlyDisplayPoweredByUppy={false}
            width={'100%'}
          />
      </div>
      </Page.Card>
      </SiteWrapper>
    )
  }
}

export default MyImageUploadPage;
