// @flow

import React, { Component } from 'react';
import Carousel, { Modal, ModalGateway } from 'react-images';

import { Page, Card, Grid, Form, Button, Dropdown } from "tabler-react";
import View from './AlternativeMedia/View';
import ComponentDemo from "./ComponentDemo";
import SiteWrapper from "./SiteWrapper.react";
// import { videos } from './data';

class FormElements extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      modalIsOpen: false,
    }
  }

  componentDidMount() {
	  let token = window.$token;
    fetch('/api/videosList', {
	  method: "GET",
	  headers: {
		  token: token
	  }
	})
    .then(res => res.json())
    .then(data => {
        // alert(data)
        let videos = data.map((item) => {
          return   {
            poster: 'https://peach.blender.org/wp-content/uploads/bbb-splash.png',
            sources: [
              {
                type: 'video/mp4',
                url: "/api/videos/" + item
              }
            ],
          }
        });
        // alert(JSON.stringify(images))
        this.setState({
          data: data,
          videos: videos
        })


    })
    .catch(e => console.log('错误:', e))
  }

  toggleModal = (e) => {
    // alert(JSON.stringify(this.state.videos));
    this.setState(state => ({ modalIsOpen: !state.modalIsOpen }));
    e.preventDefault();
  }

  render() {
    return (
      <SiteWrapper>
        <Page.Card
          title="下載"
          RootComponent={Form}
          footer={
            <Card.Footer>
              <div className="d-flex">
                <Button link>取消</Button>
                  <Button link color="info" className="ml-auto" onClick={(e) => {this.toggleModal(e)}} >
                    瀏覽
                  </Button>
                  <Button type="submit" color="primary" className="ml-auto">
                    下載
                  </Button>
              </div>
            </Card.Footer>
          }
        >
                <Form.Group>
                  <Form.ImageCheck>

                {this.state.data &&
                    this.state.data.map((item, index) => (
                      <Form.ImageCheckItem
                        key={item}
                        value={item}
//                        imageURL={"https://viot.tradevan.com.tw/veltekServiceRS/images/" + item}
                        imageURL={"/api/videos/" + item}
                      />
                    ))}

                  </Form.ImageCheck>
                </Form.Group>
        </Page.Card>
        <ModalGateway>
          {(this.state.modalIsOpen && this.state.videos) ? (
              <Modal
                allowFullscreen={true}
                closeOnBackdropClick={false}
                onClose={this.toggleModal}
              >
                <Carousel
                  currentIndex={0}
                  components={{ Footer: null, View }}
                  views={this.state.videos}
                />
              </Modal>
          ) : null}
        </ModalGateway>
      </SiteWrapper>
    );
  }
}

export default FormElements;
