// @flow

import * as React from "react";

import {
  Page,
  Avatar,
  Icon,
  Grid,
  Card,
  Text,
  Table,
  Alert,
  Progress,
  colors,
  Dropdown,
  Button,
  StampCard,
  StatsCard,
  ProgressCard,
  Badge,
  Form,
  PricingCard,
} from "tabler-react";

import C3Chart from "react-c3js";

import SiteWrapper from "./SiteWrapper.react";

function Home() {
  return (
    <SiteWrapper>
      <Page.Card
        title="首頁"
        RootComponent={Form}
      >
      	<Grid.Row cards={true}>
	      <Grid.Col sm={4}>
	        <Card statusColor="red">
	          <Card.Body>
	            <C3Chart
	              style={{ height: "11rem" }}
	              data={{
	                columns: [
	                  // each columns data
	                  ["data1", 63],
	                  ["data2", 37],
	                ],
	                type: "donut", // default type of chart
	                colors: {
	                  data1: colors["green-light"],
	                  data2: colors["gray"],
	                },
	                names: {
	                  // name of each serie
	                  data1: "剩餘容量",
	                  data2: "已使用容量",
	                },
	              }}
	              legend={{
	                show: false, //hide legend
	              }}
	              padding={{
	                bottom: 0,
	                top: 0,
	              }}
	            />
	          </Card.Body>
	        </Card>
	      </Grid.Col>
          <Grid.Col sm={8}>
	        <PricingCard active>
	          <PricingCard.Category>{"進階方案"}</PricingCard.Category>
	          <PricingCard.AttributeList>
	              <PricingCard.AttributeItem>
	                <strong>10 </strong>
	                {"位使用者"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available>
	                <strong>1,000GB </strong>
	                {"儲存空間"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available>>
	                {"圖片處理"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available={false}>
	                {"訊息通知"}
	              </PricingCard.AttributeItem>
	              <PricingCard.AttributeItem hasIcon available={false}>
	                {"上傳facebook"}
	              </PricingCard.AttributeItem>
	          </PricingCard.AttributeList>
	        </PricingCard>
	      </Grid.Col>
      	</Grid.Row>
      </Page.Card>
    </SiteWrapper>
  );
}

export default Home;
