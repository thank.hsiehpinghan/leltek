// @flow

import React, { Component } from 'react';

import { Page, Card, Table, Form, Button, Dropdown } from "tabler-react";

import ComponentDemo from "./ComponentDemo";
import SiteWrapper from "./SiteWrapper.react";
import loremIpsum from 'lorem-ipsum';
import Gallery from './react-images-texts-videos/Gallery';

const theme = {
  // container
  container: {
    background: 'rgba(255, 255, 255, 0.9)',
  },

  // arrows
  arrow: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    fill: '#222',
    opacity: 0.6,
    transition: 'opacity 200ms',

    ':hover': {
      opacity: 1,
    },
  },
  arrow__size__medium: {
    borderRadius: 40,
    height: 40,
    marginTop: -20,

    '@media (min-width: 768px)': {
      height: 70,
      padding: 15,
    },
  },
  arrow__direction__left: { marginLeft: 10 },
  arrow__direction__right: { marginRight: 10 },
  close: {
    fill: '#D40000',
    opacity: 0.6,
    transition: 'all 200ms',
    ':hover': {
      opacity: 1,
    },
  },

  // footer
  footer: {
    color: 'black',
  },
  footerCount: {
    color: 'rgba(0, 0, 0, 0.6)',
  },

  // thumbnails
  thumbnail: {
  },
  thumbnail__active: {
    boxShadow: '0 0 0 2px #00D8FF',
  },
};

class FormElements extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      modalIsOpen: false,
      files: []
    }
  }

  componentDidMount() {
	  let token = window.$token;
    fetch('/api/filesList', {
	  method: "GET",
	  headers: {
		  token: token
	  }
	})
    .then(res => res.json())
    .then(data => {
        // alert(data)
        data.map((item) => {
          fetch(
            "/api/files/" + item
          )
          .then(res => res.text())
          .then(content => {
            this.state.files.push(content)
            this.setState({
              files: this.state.files
            })
          });
        });
        // alert(JSON.stringify(images))
        // alert(this.state.files)
        this.setState({
          data: data
        })
    })
    .catch(e => console.log('错误:', e))
  }

  render() {
    return (
      <SiteWrapper>
        <Page.Card
          title="下載"
          RootComponent={Form}
          footer={
            <Card.Footer>
              <div className="d-flex">
                <Button link>取消</Button>
                <Button type="submit" color="primary" className="ml-auto">
                  下載
                </Button>
              </div>
            </Card.Footer>
          }
        >
            {this.state.files ? (
              <Gallery texts={this.state.files} theme={theme} />
              ) : null}
        </Page.Card>
      </SiteWrapper>
    );
  }
}

export default FormElements;
